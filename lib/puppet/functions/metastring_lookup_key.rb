# The `metastring_lookup_key` is a hiera 5 `lookup_key` data provider function.
#
Puppet::Functions.create_function(:metastring_lookup_key) do

  dispatch :metastring_lookup_key do
    param 'String[1]', :key
    param 'Hash[String[1],Any]', :options
    param 'Puppet::LookupContext', :context
  end

  def metastring_lookup_key(key, options, context) do
    return context.cached_value(key) if context.cache_has_key(key)

    raw_data = context.cached_value(nil)
    if raw_data.nil?
      raw_data = load_data_hash(options, context)
      context.cache(nil, raw_data)
    end
    context.not_found unless raw_data.include?(key)
    context.cache(key, parse_value(raw_data[key], context, options))
  end

  def load_data_hash(options, context) do
    path = options(path)
    context = cached_file_data(path) do |content|
      begin
        data = YAML.load(content, path)
        if data.is_a?(Hash)
          Puppet::Pops::Lookup::HieraConfig.symkeys_to_string(data)
        else
          msg = _("%{path}: file does not contain a valid yaml hash") % { path: path }
          raise Puppet::DataBinding::LookupError, msg if Puppet[:strict] == :error && data != false
          Puppet.warning(msg)
          {}
        end
      rescue YAML::SyntaxError => ex
        # Psych errors includes the absolute path to the file, so no need to add that
        # to the message
        raise Puppet::DataBinding::LookupError, "Unable to parse #{ex.message}"
      end
    end
  end

  def parse_value(value, context, options) do
    if external?(value)
      #### all the magic of the MSLK[] happens here
      # create parsing function based on set options
    end
    context.inerpolate(data)
  end

  def metastring_supported_options do
    [
      :delimiter,
      :trim_key_prefix,
      :data_source_path,
    ]
  end

  def external?(data)
    /.MSLK\[.*?\]/ =~ data ? true : false
  end
end
